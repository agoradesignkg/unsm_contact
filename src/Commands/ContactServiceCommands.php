<?php

namespace Drupal\unsm_contact\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush service commands.
 */
class ContactServiceCommands extends DrushCommands {

  /**
   * The block content storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $blockContentStorage;

  /**
   * The block plugin storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $blockPluginStorage;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ContactServiceCommands object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->blockContentStorage = $entity_type_manager->getStorage('block_content');
    $this->blockPluginStorage = $entity_type_manager->getStorage('block');
    $this->configFactory = $config_factory;
  }

  /**
   * Creates the opening hours content block and optionally places it.
   *
   * This command is kinda deprecated already, as we are switching from a block
   * to site_setting entity, as this is easier maintainable (permissions).
   *
   * @command unsm_contact:create_oh_block
   *
   * @option bool place
   *   Whether the block should be placed in a theme region. Default to FALSE.
   * @option string theme
   *   If the block should placed, then this option defines the theme. If empty,
   *   the default theme will be used.
   * @option string region
   *   The region to place the block. Defaults to 'footer_center'.
   * @option string langcode
   *   The language code. Defaults to 'de'.
   *
   * @usage unsm_contact:create_oh_block --place
   *   Creates the opening hours content block and places it into the
   *   'footer_center' region of the default theme.
   *
   * @aliases umc:cohb
   */
  public function createOpeningHoursBlock($options = ['place' => FALSE, 'theme' => '', 'region' => 'footer_center', 'langcode' => 'de']) {
    $label = 'Öffnungszeiten';
    $text = '<p>Montag bis Freitag<br/>';
    $text .= '08:30 – 12:00 und 13:00 – 17:30 Uhr<br/>';
    $text .= 'Samstags nach Vereinbarung</p>';

    /** @var \Drupal\block_content\BlockContentInterface $content_block */
    $content_block = $this->blockContentStorage->create([
      'type' => 'basic',
      'status' => 1,
      'langcode' => $options['langcode'],
      'info' => $label,
      'reusable' => 1,
      'body' => [
        'value' => $text,
        'summary' => '',
        'format' => 'basic_html',
      ],
    ]);
    $content_block->save();
    $this->output()->writeln(sprintf('Created opening hours block. Block ID: %s.', $content_block->id()));

    if ($options['place']) {
      if (empty($options['theme'])) {
        $options['theme'] = $this->configFactory->get('system.theme')->get('default');
      }
      $plugin_id = 'block_content:' . $content_block->uuid();
      $block_plugin = $this->blockPluginStorage->create([
        'id' => sprintf('%s_opening_hours', $options['theme']),
        'plugin' => $plugin_id,
        'theme' => $options['theme'],
        'region' => $options['region'],
        'weight' => 0,
        'settings' => [
          'id' => $plugin_id,
          'label' => $label,
          'provider' => 'block_content',
          'label_display' => 'visible',
          'status' => TRUE,
          'info' => '',
          'view_mode' => 'full',
        ],
      ]);
      $block_plugin->save();
      $this->output()->writeln(sprintf('Placed opening hours block.'));
    }
  }

}
